
#include "stdafx.h"
#include <windows.h>
#include "opencv2/opencv.hpp"
#include "WisFaceEngineExtend.h"

using namespace cv;

int _tmain(int argc, _TCHAR* argv[])
{
	QsHANDLE_EXTEND engine = qs_Wis_Create_Extend();

	Mat img1 = imread("1.jpg");
	if (img1.empty() == true)
	{
		printf("imread failed\n");
		return 1;
	}
	//rect 需要调用 人脸识别SDK qs_Wis_DetectFaces，返回人脸QsFace.rect,或者调用本sdk中qs_Wis_Process_Detect进行人脸定位，找到脸部的位置信息
	QsRect faces[1];
	int facenum = qs_Wis_Process_Detect(engine,img1.data, img1.cols, img1.rows, img1.step.p[0], faces,1);
	if (facenum < 1)
	{
		printf("no face in 1.jpg\n");
		return 0;
	}
	QsFaceAttr attr;
	
	qs_Wis_Process_FaceInfo(engine,img1.data, img1.cols, img1.rows, img1.step.p[0],faces[0],&attr);
	printf("age=%f, gender = %d\n",attr.age,attr.gender);
	qs_Wis_Process_Dispose(engine);
	system("pause");
	return 0;
}

