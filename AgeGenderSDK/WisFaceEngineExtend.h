#pragma once
#include <stdio.h>
#include "windows.h"

#define QsHANDLE_EXTEND long long

typedef struct {
	int 	left;			// 矩形框左上角x坐标
	int 	top;			// 矩形框左上角y坐标
	int 	right;			// 矩形框右下角x坐标
	int 	bottom;			// 矩形框右下角y坐标
} QsRect;

typedef struct  {
	int gender;			//1 man, -1 woman
	float age;
} QsFaceAttr;

/// <summary>
/// 创建人脸扩展引擎
/// </summary>
/// <param name="tag">tag = 0 </param>
/// <returns>返回人脸引擎扩展句柄engine if >0 sucess, or falied</returns>
QsHANDLE_EXTEND qs_Wis_Create_Extend();


/// <summary>
/// 人脸检测
/// </summary>
/// <param name="engine">qs_Wis_Create_Extend()返回的人脸引擎句柄</param>
/// <param name="image">照片imgBgr24</param>
/// <param name="width">图像宽度</param>
/// <param name="height">图像高度</param>
/// <param name="widthstep">widthstep是存储一行图像所占的字节（相邻两行起点指针的差值）</param>
/// <param name="faceRects">返回人脸位置</param>
/// <param name="maxFaceNum">faceRects 的数组长度</param>
/// <returns>检测到的人脸个数</returns>
int qs_Wis_Process_Detect(QsHANDLE_EXTEND handler, unsigned char* imgBgr24, int width, int height, int widthstep, QsRect* faceRects, int maxFaceNum);


/// <summary>
/// 人脸属性检测
/// </summary>
/// <param name="engine">qs_Wis_Create_Extend()返回的人脸引擎句柄</param>
/// <param name="image">照片imgBgr24</param>
/// <param name="width">图像宽度</param>
/// <param name="height">图像高度</param>
/// <param name="widthstep">widthstep是存储一行图像所占的字节（相邻两行起点指针的差值）</param>
/// <param name="face">输入人脸的位置，调用qs_Wis_DetectFaces，返回人脸QsFace.rect</param>
/// <param name="attr">返回人脸属性</param>
/// <returns>0 success</returns>
int qs_Wis_Process_FaceInfo(QsHANDLE_EXTEND handler, unsigned char* imgBgr24, int width, int height, int widthstep, QsRect faceRect, QsFaceAttr *attr);

/// <summary>
/// 释放资源
/// </summary>
void qs_Wis_Process_Dispose(QsHANDLE_EXTEND handler);
